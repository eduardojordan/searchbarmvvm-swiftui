//
//  DetailView.swift
//  SerachBarrMVVM
//
//  Created by Eduardo Jordan on 22/4/24.
//

import SwiftUI

struct DetailView: View {
    @State var user : User
    
    var body: some View {
        VStack {
            Image(systemName: "person")
                .resizable()
                .frame(width: 50,height: 50)
                .foregroundColor(.white)
                .padding(12)
                .background(Color.black)
                .clipShape(Circle())
                .padding(20)
            VStack {
                Text("User: \(user.name)")
                Text("Email: \(user.email)")
                Text("Phone: \(user.phone)")
                Text("Company: \(user.company.name)")
                Text("City: \(user.address.city)")
            }
            Spacer()
        }
    }
}

#Preview {
    DetailView(user: User(id: 12, name: "Maria", username: "MariaOne", email: "maria@gmail.com", address: Address(street: "12", suite: "32", city: "MAracaibo", zipcode: "4001", geo: Geo(lat: "12.43", lng: "-102.23")), phone: "303000333", website: "www.maria.com", company: Company(name: "Maria Company", catchPhrase: "", bs: "") ))
}
