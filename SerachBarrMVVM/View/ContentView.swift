//
//  ContentView.swift
//  SerachBarrMVVM
//
//  Created by Eduardo Jordan on 22/4/24.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject private var viewModel = UsersViewModel()
    @State private var searchQuery = ""
    
    var body: some View {
        NavigationStack{
            List(viewModel.filterUsers, id: \.id ) { users in
                
                NavigationLink {
                    DetailView(user: users)
                } label: {
                    HStack{
                        Image(systemName: "person")
                            .resizable()
                            .frame(width: 20,height: 20)
                            .foregroundColor(.white)
                            .padding(12)
                            .background(Color.black)
                            .clipShape(Circle())
                        VStack(alignment: .leading) {
                            Text(users.name)
                            Text(users.email)
                        }
                        .font(.headline)
                    }
                }
            }
            .searchable(text: $searchQuery,prompt: "Search user")
            .onChange(of: searchQuery) { _ , query in
                viewModel.getFilterUSer(query:query)
            }
            .navigationTitle("Users")
        }
    }
}

#Preview {
    ContentView()
}
