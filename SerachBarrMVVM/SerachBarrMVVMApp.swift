//
//  SerachBarrMVVMApp.swift
//  SerachBarrMVVM
//
//  Created by Eduardo Jordan on 22/4/24.
//

import SwiftUI

@main
struct SerachBarrMVVMApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
