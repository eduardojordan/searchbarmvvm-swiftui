//
//  UsersViewModel.swift
//  SerachBarrMVVM
//
//  Created by Eduardo Jordan on 22/4/24.
//

import Foundation

class UsersViewModel: ObservableObject {
    @Published var users: [User] = []
    @Published var filterUsers: [User] = []
    
    init(){
        Task {
            await getUsers()
        }
    }
    
    func getUsers() async {
        let url = URL(string: "http://jsonplaceholder.typicode.com/users")!
        let (data, _) = try! await URLSession.shared.data(from: url)
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        
        DispatchQueue.main.async {
            self.users = try! decoder.decode([User].self, from: data)
            self.filterUsers = self.users
        }
    }
    
    func getFilterUSer(query: String) {
        if query.isEmpty {
            filterUsers = users
        }else{
            filterUsers = users.filter { user in
                user.name.lowercased().contains(query.lowercased())
                
            }
        }
    }
    
}
